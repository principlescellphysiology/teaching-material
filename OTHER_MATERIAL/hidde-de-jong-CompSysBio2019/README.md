Practical exercises: Modelling integrated networks of metabolism and gene expression
------------------------------------------------------------------------------------

* held by: Hidde de Jong
* held at: [CompSysBio - Advanced Lecture Course on Computational Systems Biology](https://project.inria.fr/compsysbio2019/), Aussois 2019  
* held in: Spring 2019

-----------------------
### Dynamic models integrating metabolism and gene expression

Hidde de Jong

INRIA Grenoble – Rhône-Alpes

We will step-by-step build quantitative ODE models of a metabolic network integrating regulation on both the metabolic and gene expression level, and investigate the effect of these layers of regulation on the networks dynamics. The course will be structured around the case of carbon catabolite repression in bacteria, using the simple models described in Kremling et al. (2015). All simulations will be carried out by means of Matlab (no toolboxes needed), using code provided by the instructor.

Kremling A, Geiselmann J, Ropers D, de Jong H (2015). Understanding carbon catabolite repression in Escherichia coli using quantitative models. Trends Microbiol., 23:99-109