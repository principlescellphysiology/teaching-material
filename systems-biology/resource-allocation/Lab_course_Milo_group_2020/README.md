Lab course "Resource allocation models"
---------------------------------------

* held by: W. Liebermeister
* held at: online, for PhD students of the Milo group, Weizmann Institute of Science
* held in: Spring 2020

Notes
* The session was part of a larger course held together with E. Noor
