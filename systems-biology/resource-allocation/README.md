### Teaching materials "Resource allocation and enzyme economics"

* [Lecture "Whole-cell models"](PSL-ITI_Whole_Cell_Modeling_2017) (W. Liebermeister, 2017) 
* [Online course "Resource allocation models"](Lab_course_Milo_group_2020) (W. Liebermeister) 
* [Lecture slides "Resource allocation and growth"](SysBio_2021_Lecture_5_Bertaux_Resource_Allocation.pdf) (F. Bertaux, CRI 2021)
* [Blackboard session "Enzyme economy in metabolic models" 2021](CompSysBio2021) (W. Liebermeister)
