### Teaching materials "Economy of the cell"

Lectures (EPCP summer school Paris 2022)

* [Cellular economics](epcp-summer-school-2022/EPCP_Paris_2022_Lecture-1-INTRO_Introduction.pdf) (W. Liebermeister)
* [What makes up a cell?](epcp-summer-school-2022/EPCP_Paris_2022_Lecture-2-CEN_What_makes_up_a_cell.pdf) (D. Szeliova)
* [Optimality in cells](epcp-summer-school-2022/EPCP_Paris_2022_Lecture-3-OPT_Optimality.pdf) (M. Köbis)
* [A dynamic view of metabolism](epcp-summer-school-2022/EPCP_Paris_2022_Lecture-4-MET_Dynamic_metabolism.pdf) (O. Soyer)
* [Flux balance analysis](epcp-summer-school-2022/EPCP_Paris_2022_Lecture-6-FLX_FBA.pdf) (S. Waldherr)
* [Cost of metabolic pathways](epcp-summer-school-2022/EPCP_Paris_2022_Lecture-7-PAT_Cost_of_metabolic_pathways.pdf) (E. Noor)
* [Optimal metabolic states](epcp-summer-school-2022/EPCP_Paris_2022_Lecture-8-OME_Optimal_metabolic_states.pdf) (M. Wortel)
* [Self-replicator cell models](epcp-summer-school-2022/EPCP_Paris_2022_Lecture-9-SMA_Self_replicator_cell_models.pdf)
  [(Notes)](epcp-summer-school-2022/EPCP_Paris_2022_Lecture-9-SMA_Self_replicator_cell_models-NOTES.pdf)  (O. Golan)
* [Resource allocation models](epcp-summer-school-2022/EPCP_Paris_2022_Lecture-10-LAR_RBA.pdf) (A. Goelzer)
* [Optimal cell behavior in time](epcp-summer-school-2022/EPCP_Paris_2022_Lecture-11-DYN_Optimal_behavior_in_time.pdf) (H. de Jong)

Exercises

* [Cell components](epcp-summer-school-2022/EPCP_Paris_2022_Exercise_CEN_What_makes_up_a_cell.pdf) (D. Szeliova)
* [Balanced growth](epcp-summer-school-2022/EPCP_Paris_2022_Exercise_BAL_Balanced_cell_growth.pdf) (M. Wortel)
* [FBA (jupyter notebook)](epcp-summer-school-2022/EPCP_Paris_2022_Exercise_FLX_Flux_balance_analysis.ipynb) (S. Waldherr)
* [Pathway fluxes and cost](epcp-summer-school-2022/EPCP_Paris_2022_Exercise_PAT_Cost_of_metabolic_pathways.pdf) (E. Noor)
* [Optimal metabolic states](epcp-summer-school-2022/EPCP_Paris_2022_Exercise_OME_Optimal_metabolic_states.pdf) (M. Wortel)

Lectures (EPCP summer school Paris 2023)

* [Economy of the cell](epcp-summer-school-2023/EPCP_Paris_2023_Lecture-01-A-Exploring-Meike.pdf) (Meike Wortel)
* [What makes up a cell?](epcp-summer-school-2023/EPCP_Paris_2023_Lecture-02-CEN-Diana.pdf) (Diana Szeliova)
* [Autocatalytic cycles and growth laws](epcp-summer-school-2023/EPCP_Paris_2023_Lecture-03-AUT-Rami.pdf) (Rami Pugatch)
* [Self-replicating cell model](epcp-summer-school-2023/EPCP_Paris_2023_Lecture-04-SMA-Andrea.pdf) (Andrea Weiße)
* [Flux Balance Analysis](epcp-summer-school-2023/EPCP_Paris_2023_Lecture-05-CBM-FLX-Steffen.pdf) (Steffen Waldherr)
* [Metabolic dynamics](epcp-summer-school-2023/EPCP_Paris_2023_Lecture-06-MET-Orkun.pdf) (Orkun Soyer)
* [Variability in cell populations (I)](epcp-summer-school-2023/EPCP_Paris_2023_Lecture-07-VAR-A-Andrea.pdf) (Andrea de Martino)
* [Variability in cell populations (II)](epcp-summer-school-2023/EPCP_Paris_2023_Lecture-07-VAR-B-Daniele.pdf) (Daniele de Martino)
* [Return on investment](epcp-summer-school-2023/EPCP_Paris_2023_Lecture-08-ROI-Hyun.pdf) (Hyun-Seob Song)
* [Cell division control](epcp-summer-school-2023/EPCP_Paris_2023_Lecture-09-CDC-Mattia.pdf) (Mattia Corigliano)
* [Cells facing uncertainty (I)](epcp-summer-school-2023/EPCP_Paris_2023_Lecture-10-UNC-A-Olivier.pdf) (Olivier Rivoire)
* [Cells facing uncertainty (II)](epcp-summer-school-2023/EPCP_Paris_2023_Lecture-10-UNC-B-David.pdf) (David Lacoste)
* [Organ scaling and function](epcp-summer-school-2023/EPCP_Paris_2023_Lecture-11-ORG-Frederique-Cyril.pdf) (Frederique Noel and Cyril Karamaoun)
