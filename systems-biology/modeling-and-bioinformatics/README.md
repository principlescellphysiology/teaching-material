### Teaching materials "Mathematical modeling and bioinformatics"

* [Course script on dynamical systems](Dynamical_Systems_WS2009.pdf) (HU Berlin Student course, 2009)
* [Lecture script "Modeling formalisms besides kinetic models" (in German)](./systems-biology/cellular-networks/Lecture_Modelling_of_cell_processes_2009/course_script/script_3_other_formalisms_german.pdf) (W. Liebermeister, 2009) 
* [Blackboard session on multistability, bifurcation, and differentiation](multistability) (D. Gonze, blackboard session at CompSysBio 2021 workshop) 
* [Course script on parameter estimation and model selection](Parameter_Estimation_Model_discrimination_WS2009.pdf) (HU Berlin Student course, 2009)
* [Lecture script "Model reduction and coupling"](./systems-biology/cellular-networks/Lecture_Modelling_of_cell_processes_2009/course_script/script_6_model_reduction_and_coupling.pdf) (W. Liebermeister, 2009) 
* [Course script on stochastic processes](Stochastic_processes_SS2009.pdf) (HU Berlin Student course, 2009)
* [Course script on sequence analysis](Sequence_Analysis_SS2005.pdf) (HU Berlin Student course, 2005)
