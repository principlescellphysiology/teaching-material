<html>

<center>

<table BORDER=0 WIDTH=700><tr><td>

<hr color="#0000DD" size=2>
<center><font size=+2><b>Multistability, bifurcations, and differentiation: Practicals</b></font></center>
<center><font size=+1><a href="https://www2.ulb.ac.be/sciences/utc/home.html">Didier Gonze</a></font></center>
<hr color="#0000DD" size=2>
<br>

<h2>Introduction</h2>

During embryonic development, cells divide and successively differentiate to acquire specific identities. This differentiation process is controlled by gene regulatory networks that typically display bi- or multistability. We will see how to build ODE models for such gene regulatory networks and how to analyze these systems using numerical methods and bifurcation theory. We will then describe a particular model recently developed to account for cell fate specification during early embryonic development in mammals (De Mot et al, 2016). The single cell version as well as coupled cells versions will be considered (see <a href="Aussois2021.pdf">Aussois2021.pdf</a> for details). 
<br>
<br>
These models will be simulated with Matlab (<a href="https://fr.mathworks.com">https://fr.mathworks.com</a>) and XPP-AUTO (Bart Ermentrout, <a href="http://www.math.pitt.edu/~bard/xpp/xpp.html">http://www.math.pitt.edu/~bard/xpp/xpp.html</a>). A more detailed tutorial of XPP-AUTO is available <a href="http://homepages.ulb.ac.be/~dgonze/TEACHING/tutorial_xpp/tutorial_xpp.html">here</a>.

<br><br>


<br><h2>Toggle switch</h2>

To get familiarized with the numerical analysis of non-linear systems, we will start with a simple model, known as the Toggle Switch (Gardner et al, 2000). 

<br><img src="toggle_eqs.png" height=120><br><br>

The dynamics of this 2-variable system will be investigated with the XPP-AUTO software. Here is a step-by-step approach to illustrate how to produce time series, to visualize the dynamics in the phase plane and to build bifurcation diagrams.

<br><br>
<b><u>ODE file</u></b>

<br><br>

The first step is to create a text file (with the extension .ode) that contains the equations, parameter values, and initial conditions. Other informations including integration and plotting settings can also be specified.
<br><br>


<table width=400 cellpadding=8>
<tr><td bgcolor="CCCCCC"><b><a href="toggle.ode">toggle.ode</a></b></td></tr>
<tr>
<td bgcolor="DDDDDD">
<code>
# Toggle Switch (Gardner et al, 2000)
<br>
<br>dX/dt=v1*Ki^n/(Ki^n+Y^n)-k1*X
<br>dY/dt=v2*Ki^n/(Ki^n+X^n)-k2*Y
<br>
<br>param v1=2, v2=2
<br>param k1=1, k2=1
<br>param Ki=1, n=4
<br>
<br>ini x=0.1,y=0.2
<br>
<br>@ total=100, dt=0.01
<br>@ nplot=2, yp1=X, yp2=Y
<br>
<br>done
<br>
</code>
</td></tr></table>

<br>
The lines starting with # are comments. Then comes the equations, parameter values and initial conditions. The <i>ini</i> statement is optional (by default initial conditions are set to zero). We have also specified some integration parameters: <i>total</i> = total time (default: 20) and <i>dt</i> = time step (default: 0.05), as well as some instructions for the plot (the time curves for X and Y will be displayed on the screen). 

<br><br>
<b><u>Launching XPP</u></b>

<br><br>

XPP can now be launched with this ode file. Depending on how it is configured, you can either launch XPP via the command line (> xpp toggle.ode) or you can launch xpp and then select the ode file in the "browsing" window that opens).
<br><br>

<b><u>Computing the solution:</u></b>

<br><br>

In the Main Window, you see an empty graph, some menus on the left and some buttons on the top. The title of the graphs says X vs T, which tells you that the variable X is along the vertical axis and T along the horizontal axis. The plotting range is from 0 to 20 along the horizontal and -1 to 1 along the vertical axis. 

<br><br>

Click on <b>Initialconds</b> > <b>Go</b> (keyboard shortcuts: <font style="border: 1px solid">&nbsp;I&nbsp;</font> <font style="border: 1px solid">&nbsp;G&nbsp;</font> ) in the Main Window. The solution will be drawn  as time series. You can change the axes limits by going in the menu <b>Viewaxes</b> ( <font style="border: 1px solid">&nbsp;V&nbsp;</font> ). Select <b>2D</b> ( <font style="border: 1px solid">&nbsp;ENTER&nbsp;</font> ) and change the settings. Alternatively, you can fit automatically the axes by clicking on <b>Window/zoom</b> > <b>Fit</b> ( <font style="border: 1px solid">&nbsp;W&nbsp;</font> <font style="border: 1px solid">&nbsp;F&nbsp;</font> ). The time curve for X is drawn in black and the time curve for Y is shown in red. 

<br><br>

The simulation stops at the time specified by "Total" in the ode file (default: 20). This time can also be changed in menu <b>nUmerics</b> > <b>Total</b> ( <font style="border: 1px solid">&nbsp;U&nbsp;</font> <font style="border: 1px solid">&nbsp;T&nbsp;</font> ). You can also continue the simulation from the last point reached by selecting <b>Initialconds</b> > <b>Last</b> ( <font style="border: 1px solid">&nbsp;I&nbsp;</font> <font style="border: 1px solid">&nbsp;L&nbsp;</font> ). The final point of the simulation then becomes the new initial condition. 

<br><br>

<center><img src="toggle_ts.png" height=220></center>

<br><br>

<b><u>Initial conditions</u></b>

<br><br>

Initial conditions can be changed via the window <b>ICs</b> (see menus on the top of the graph). Run simulations for different initial conditions and check the occurrence of bistability.

<br><br>

<b><u>Phase plane</u></b>

<br><br>

An alternative representation is the phase plane (variable X vs variable Y). It can be displayed as follows: go in menu <b>Viewaxes</b> and click on <b>2D</b> ( <font style="border: 1px solid">&nbsp;V&nbsp;</font>&nbsp;<font style="border: 1px solid">&nbsp;ENTER&nbsp;</font> ). In the window that pops up, type "X" as X-axis and  "Y" as Y-axis. Set the limits as follows: Xmin = Ymin = 0 and Xmax = Ymax = 2.5. 

<br><br>

To compute and display the nullclines, go in menu <b>Nullclines</b> and click on <b>New</b> ( <font style="border: 1px solid">&nbsp;N&nbsp;</font>&nbsp;<font style="border: 1px solid">&nbsp;N&nbsp;</font> ). Two curves appear: an orange curve corresponding to the X nullcline and a green curve corresponding to the Y nullcline. Their intersection(s) correspond(s) to the steady state(s) of the system.

<br><br>

You can run a simulation ( <font style="border: 1px solid">&nbsp;I&nbsp;</font> <font style="border: 1px solid">&nbsp;G&nbsp;</font> ). The trajectory of the system, starting from the specified initial condition, is drawn. Alternatively, you can go in menu <b>Initialconds</b> and select <b>Mouse</b> ( <font style="border: 1px solid">&nbsp;I&nbsp;</font> <font style="border: 1px solid">&nbsp;M&nbsp;</font> ). Then click on a point in the phase plane. This point will be taken as the initial condition and the trajectory of the system, starting from that initial condition, is drawn. With <b>Initialconds</b> > <b>mIce</b> ( <font style="border: 1px solid">&nbsp;I&nbsp;</font> <font style="border: 1px solid">&nbsp;I&nbsp;</font> ), you can repeatedly click on various initial conditions. Press <font style="border: 1px solid">&nbsp;ESC&nbsp;</font> to leave this mode.

<br><br>

<center><img src="toggle_pp.png" height=220></center>

<br><br>

<b><u>Parameter values</u></b>

<br><br>

Parameter values can be changed via the window <b>Param</b> (see menus on the top). Set for example v1 to 1.5. Then, assuming that you are still displaying the phase plane, click on <font style="border: 1px solid">&nbsp;E&nbsp;</font> to erase the screen and then <font style="border: 1px solid">&nbsp;N&nbsp;</font>&nbsp;<font style="border: 1px solid">&nbsp;N&nbsp;</font> to recompute the nullclines. What do you observe? Repeat this operation for v1=1.3 (or other values of your choice). To better see the effect of a parameter, you can also use the sliders below the graph. Click on <b>Par/Var?</b>. A window opens, allowing you to enter some parameters (e.g. Par/Var: v1, Value: 2, Low:1, High: 4). Click on <b>Ok</b> and then use the slider to observe the effect of that parameter on the nullcline/behaviour of the system.

<br><br>

Feel free to change other parameter values and look at the effect of these changes on the behaviour of the system, either using time series or phase plane plots.


<br><br><br>

<b><u>Bifurcation</u></b>

<br><br>

Bifurcation diagrams can be build with AUTO. Before launching AUTO, let's restore the default settings. In the <b>IC</b> and <b>Param</b> windows, click on <b>Default</b>. Then set the axes to plot X as a function of Time. The bifurcation diagram will be numerically computed from a steady state. You thus need to make the system converge to a (stable) steady state by running a simulation and make sure it converged to a steady state ( <font style="border: 1px solid">&nbsp;I&nbsp;</font> <font style="border: 1px solid">&nbsp;G&nbsp;</font> <font style="border: 1px solid">&nbsp;I&nbsp;</font> <font style="border: 1px solid">&nbsp;L&nbsp;</font> <font style="border: 1px solid">&nbsp;I&nbsp;</font> <font style="border: 1px solid">&nbsp;L&nbsp;</font>... until no change of X are discernible). 

<br><br>

Then you can launch AUTO: menu <b>File</b> > <b>Auto</b> ( <font style="border: 1px solid">&nbsp;F&nbsp;</font> <font style="border: 1px solid">&nbsp;A&nbsp;</font> ). A new window opens with a graph of the first variable (here: X) as a function of the first parameter (here: v1). In the menu <b>Parameter</b> ( <font style="border: 1px solid">&nbsp;P&nbsp;</font> ), you can change the parameter used for the bifurcation diagram (Par1). Here we will keep v1. In the menu <b>Axes</b>, you can change the variable you want to display on the graph (<b>hI-lo</b>; <font style="border: 1px solid">&nbsp;A&nbsp;</font> <font style="border: 1px solid">&nbsp;ENTER&nbsp;</font> ) and some other features. For the moment, we will keep it with the default settings. In the menu <b>nUmerics</b> ( <font style="border: 1px solid">&nbsp;U&nbsp;</font> ), some numerical parameters can be changed. Here we will change Par Max. Since we want to build the bifurcation diagram for values of v1>2, we will change the default max value (2) to 10. 

<br><br>

Then, in menu <b>Run</b>, you can click on <b>Steady state</b> ( <font style="border: 1px solid">&nbsp;R&nbsp;</font> <font style="border: 1px solid">&nbsp;S&nbsp;</font> ) to ask AUTO to build the bifurcation diagram (more specifically to extend the steady state branch through continuation methods). To fit the axes limits, you can go to the menu <b>Axes</b> and click on <b>Fit</b> ( <font style="border: 1px solid">&nbsp;A&nbsp;</font> <font style="border: 1px solid">&nbsp;F&nbsp;</font> ). A "S" curve appears on the screen. The stable steady state branches appear in red and the unstable ones in black. The region of bistability is delimited by two limit points (LP) (= Saddle-Node bifurcations). Note that the diagram is not complete. AUTO started from the given steady state and extended the branch "to the right" (i.e. increasing value of v1). To get the "left part" of the diagram, you can go back in <b>nUmerics</b> and change the sign of "dS" (i.e. set dS=-0.02). Then click on <b>Grasp</b>, which allows to select a point of the bifurcation diagram as a new starting point. With <font style="border: 1px solid">&nbsp;Tab&nbsp;</font> or with the left/right arrows, you can move the cursor (cross) along the bifurcation diagram. Select the first point (label "1") and click on  <font style="border: 1px solid">&nbsp;ENTER&nbsp;</font>. Once done, click on <b>Run</b> and then refit the axis (menu <b>Axes</b> > <b>Fit</b>). You should now see the complete bifurcation diagram.

<br><br>

<center><img src="toggle_bif.png" height=220></center>

<br><br>




<br><br><h2>De Mot model - 1 cell</h2>

De Mot et al (2016) devised a 4-variable model to describe the dynamics of the gene regulatory network (GRN) governing the cell specification during the early embryogenesis in mammals. The GRN is centered on Gata6/Nanog and includes the ERK/Fgf4 signalling. Cells from the inner cell mass (ICM) specify either into Epiblast (Epi), characterized by a high level of Nanog and a low level of Gata6, or into Primitive Endoderm (PrE), characterized by a high level of Gata6 and a low level of Nanog. In the latter cells ERK/Fgf4 signalling is also active.

<br><br><center><img src="scheme_GRN.png" height=200></center>

<br><img src="demot_eqs.png" height=200><br><br>

We will first study the occurrence of tristability in this system at the single-cell level, and then we will assess the role of the Fgf4-mediated coupling in the emergence of 2 cell types with a 2-cell model and with a 25-cell model.

<br><br>

Here is the ode file for the single-cell model.

<br><br>

<table width=700 cellpadding=8>
<tr><td bgcolor="CCCCCC"><b><a href="demot1cell.ode">demot1cell.ode</a></b></td></tr>
<tr>
<td bgcolor="DDDDDD">
<code>
# De Mot model (1 cell, 4 variables)
<br>
<br>dG/dt=(vsg2*G^s/(Kag2^s+G^s)+vsg1*(ERK^r/(Kag1^r+ERK^r)))*Kig^q/(Kig^q+N^q)-kdg*G
<br>dN/dt=(vsn2*N^v/(Kan^v+N^v)+vsn1*(Kin1^u/(Kin1^u+ERK^u)))*Kin2^w/(Kin2^w+G^w)-kdn*N
<br>dFR/dt=vsfr2*(G/(G+Kafr))+vsfr1*(Kifr/(Kifr+N))-kdf*FR
<br>dERK/dt=va*FR*(Fp/(Fp+Kd))*((1-ERK)/(1-ERK+Ka))-vi*(ERK/(ERK+Ki))
<br>
<br>param Fp=0.06
<br>
<br>param vsg1=1.202, vsg2=1
<br>param vsn1=0.856, vsn2=1
<br>param vsfr1=2.8, vsfr2=2.8
<br>param va=20, vi=3.3
<br>param kdg=1, kdn=1
<br>param kdf=1
<br>param Kag1=0.28, Kag2=0.55
<br>param Kan=0.55
<br>param Kin2=2, Kig=2
<br>param Kin1=0.28
<br>param Kafr=0.5, Kifr=0.5
<br>param Ka=0.7, Ki=0.7
<br>param Kd=2
<br>param r=3, s=4, q=4
<br>param u=3, v=4, w=4
<br>
<br>ini N=0, G=0
<br>ini FR=2.8, ERK=0.25
<br>
<br>@ nplot=2, yp1=G, yp2=N
<br>@ total=100, dt=0.01
<br>
<br>done
<br>
</code>
</td></tr></table>

<br>

In this system the control parameter is the extracellar concentration of Fgf4 (denoted here by the parameter <i>Fp</i>). Study, using time series, phase space representations, and bifurcation diagrams the dynamics of the system (and in particular its ability to produce 2 (or 3?) cell types) as a function of <i>Fp</i>.

<br><br>

You can then simulate a variation of the Fgf4 concentration on the dynamics of the system. You can impose an arbitrary profile of Fgf4 by replacing, in the ode file, the line "param Fp=0.06" by a piece-wise linear function as follows:
<br>
<code>
<br>Fp=F1+(F2-F1)/(t2-t1)*(t-t1)*heav(t-t1)*heav(t2-t)+(F2-F1)*heav(t-t2)
<br>
<br>param F1=0.06
<br>param F2=0.04
<br>param t1=30
<br>param t2=60
<br>
</code>
<br>

To visualize the profile of Fp, you have to define an auxiliary variable:
<br>
<code>
<br>aux Fgf4=10*Fp
<br>
<br>@ nplot=3, yp1=G, yp2=N, yp3=Fgf4
<br>
</code>
<br>
NB: We multiplied Fp by a factor 10 to better visualize its dynamics.
<br><br>
Feel free to change the parameters of the Fp function (to simulate for example an increase of Fgf4 and make the cell evolving towards the other steady state) or to change the function itself.
<br><br>


<br><br><h2>De Mot model - 2 cells</h2>

In the above simulations, the fate of the cell is determined by the arbitrary Fgf4 function. Now we would like to reproduce the spontaneous emergence of 2 cell types in a system of (quasi-)identical cells. Let's first consider a 2-cell system. In this system, Fp is not an arbitrary parameter anymore but a variable. We assume that each cell produces and excretes Fgf4 as function of its level of Nanog (see equations for dF1/dt and dF2/dt). In the extracellular medium, the concentration of Fgf4 is averaged: F=(Fp1+Fp2)/2. We then assume that each cell perceives the Fgf4 (F1 and F2), with a slight variability (denoted by the parameter gamma). This variability accounts for the fact that Fgf4 is not homogeneously distributed in the extracellular medium and/or results from slight cell-cell differences in the number of receptors. 

<br><br>

<table width=720 cellpadding=8>
<tr><td bgcolor="CCCCCC"><b><a href="demot2cells.ode">demot2cells.ode</a></b></td></tr>
<tr>
<td bgcolor="DDDDDD">
<code>
# De Mot model (2 cells, 10 variables) 
<br>
<br>dG1/dt=(vsg2*G1^s/(Kag2^s+G1^s)+vsg1*(ERK1^r/(Kag1^r+ERK1^r)))*Kig^q/(Kig^q+N1^q)-kdg*G1
<br>dN1/dt=(vsn2*N1^v/(Kan^v+N1^v)+vsn1*(Kin1^u/(Kin1^u+ERK1^u)))*Kin2^w/(Kin2^w+G1^w)-kdn*N1
<br>dFR1/dt=vsfr2*(G1/(G1+Kafr))+vsfr1*(Kifr/(Kifr+N1))-kdfr*FR1
<br>dERK1/dt=va*FR1*(F1/(F1+Kd))*((1-ERK1)/(1-ERK1+Ka))-vi*(ERK1/(ERK1+Ki))
<br>
<br>dG2/dt=(vsg2*G2^s/(Kag2^s+G2^s)+vsg1*(ERK2^r/(Kag1^r+ERK2^r)))*Kig^q/(Kig^q+N2^q)-kdg*G2
<br>dN2/dt=(vsn2*N2^v/(Kan^v+N2^v)+vsn1*(Kin1^u/(Kin1^u+ERK2^u)))*Kin2^w/(Kin2^w+G2^w)-kdn*N2
<br>dFR2/dt=vsfr2*(G2/(G2+Kafr))+vsfr1*(Kifr/(Kifr+N2))-kdfr*FR2
<br>dERK2/dt=va*FR2*(F2/(F2+Kd))*((1-ERK2)/(1-ERK2+Ka))-vi*(ERK2/(ERK2+Ki))
<br>
<br>dFp1/dt=vsf0+vsf*N1^z/(N1^z+Kaf^z)-kdf*Fp1
<br>dFp2/dt=vsf0+vsf*N2^z/(N2^z+Kaf^z)-kdf*Fp2
<br>
<br>F=(Fp1+Fp2)/2
<br>
<br>F1=(1-gamma)*F
<br>F2=(1+gamma)*F
<br>
<br>aux F=F
<br>aux F1=F1
<br>aux F2=F2
<br>
<br>ini G1=0, G2=0 
<br>ini N1=0, N2=0
<br>ini FR1=2.8, FR2=2.8
<br>ini ERK1=0.25, ERK2=0.25
<br>ini Fp1=0.066, Fp2=0.066
<br>
<br>par gamma=0.03
<br>
<br>par vsg1=1.202, vsg2=1
<br>par vsn1=0.856, vsn2=1
<br>par vsfr1=2.8, vsfr2=2.8
<br>par va=20, vi=3.3
<br>par kdg=1, kdn=1
<br>par kdfr=1
<br>par Kag1=0.28, Kag2=0.55
<br>par Kan=0.55
<br>par Kin2=2, Kig=2
<br>par Kin1=0.28
<br>par Kafr=0.5, Kifr=0.5
<br>par Ka=0.7, Ki=0.7
<br>par Kd=2
<br>
<br>par r=3, s=4, q=4
<br>par u=3, v=4, w=4
<br>
<br>par vsf=0.6
<br>par z=4
<br>par Kaf=5
<br>par kdf=0.0825
<br>par vsf0=0
<br>
<br>@ total=100, dt=0.01
<br>@ nplot=3, yp1=G1, yp2=G2, yp3=F
<br>
<br>done
<br>
</code>
</td></tr></table>
<br>

Parameter gamma plays a critical role. It is responsible for the small heterogeneity necessary to initiate the differentiation. You can check that if gamma is set to 0, the 2 cells remain on the intermediary (ICM) steady state. Indeed, in these conditions the system is completely symmetric and, in absence of noise, there is no way for the cells to follow different trajectories. Determine the minimum value of gamma required for the cells to differentiate.

<br><br>

Look at the time series (in case of differentiation) and check that one cell converges to the Epi state while the other cell evolves towards the PrE state. Which cell differentiates first? Do you understand the mechanism underlying the differentiation in this model? Hint: refer to the bifurcation diagram obtained for the single cell model.

<br><br>

Introducing heterogeneity via gamma is somewhat arbitrary. Cell-cell variability may also arise at the level of gene expression. Modify the equations (and consequently the ode file) to introduce a small difference in the synthesis rate of Gata6 (or Nanog) instead of gamma and check if this can induce cell specification.

<br><br>

Experimentally, the key roles of Nanog and Gata6 in cell specification were identified by studying the behaviour of knock-out mutants and by analyzing the response of the embryos to treatments interferring with ERK signalling. How would you simulate the Nanog mutant? What is happening in this mutant? Does it agree with your expectation? Which parameter would you change (set to 0) to simulate the effect of an ERK inhibitor? Simulate the effect of such an inhibitor in the Nanog mutant when it is administrated from the very beginning (i.e. at t=0) or a bit later (e.g. at t=1, t=1.5, or t=2). What do you observe? How do you explain this observation?

<br>


<br><br><h2>De Mot model - 25 cells</h2>

We have seen that the proposed mechanism allows the spontaneous differentiation in a system of 2 coupled cells. We can now consider a population of 25 cells to simulate the emergence of two cell populations. The cells will be displayed on a square lattice (5x5). The intra-cellular dynamics is governed by the same equations as above. Each cell produces and releases Fgf4 as a function of its level of Nanog. We will assume that each cell perceives the Fgf4 released by itself and by its 4 neighbours. This assumption accounts for the fact that the cell are densely packed and Fgf4 hardly diffuses in the inter-cellular space. Thus the Fgf4 concentration sensed by a cell is the average concentration of the Fgf4 produced by this cell and the 4 neighbouring cells (with periodic boundary conditions). 

<br><br><center><img src="scheme_25cells.png" height=270></center>

<br>

The matlab code to simulate this system is available here: <a href="demot25cells.m">demot25cells.m</a>. A slightly adapted version for Octave is provided here: <a href="demot25cells_octave.m">demot25cells_octave.m</a>


<br><br>
Run a simulation and check if it reproduces the salt-and-pepper pattern as depicted above. How many Epi/PrE/undifferentiated cells do you count? You can then run the program several times and assess how reproducible are the proportions of cells of each type.

<br><br>

Parameter kdf characterizes the degradation rate of Fgf4. Its default value is kdf=0.0825. Change this value to 0.05. How are the proportions affected?

<br><br>

To allow the cells to differentiate, a small source of heterogeneity is needed. In this model, cell-cell variability is incorporated via the parameter gamma, whose the value is randomly taken in a given range, with a maximum absolute value given by varmax. Check that if varmax is set to 0 (meaning that all cells are identical), no differentiation occurs. What is happening for very small values of gamma? For large values of gamma?

<br><br>

An experimental approach to investigate the mechanism of embryonic cell differentiation is to apply treatments which interfer with the Fgf4/ERK signalling. These treatments include adding exogenous Fgf4 or ERK inhibitor during a certain period of time. You can adapt the matlab code to simulate such treatments. Addition of exogenous Fgf4 can be simulated via the parameter vex and ERK inhibitor can be simulated by decreasing (or setting to 0) the parameter va. You can then study the influence of the timing and duration of these treatments on the outcome (i.e. on the proportion of cells of each type at the end of the simulation).

<br>

<br><br><h3>Concluding remarks</h3>

The next step towards a more realistic model is to consider a population of dividing cells in 3D. Such extension was implemented by Tosenberger et al (2017). Similar models have been investigated by Saiz et al (2020) or by Cang et al (2021).

<br><center><img src="simulation3D.png" height=90></center>

<br>

Such models allow to confirm the validity of the proposed mechanism under more realistic conditions, to test its robustness towards molecular noise and small cell-cell variablity, or to reproduce various experimental observations (e.g. treatments interfering with Fgf4/ERK signalling, removal/transplantation of cell(s), etc). 

<br>


<br><br><h3>References</h3>


<small>
<ul>
<li>Bessonnard S, De Mot L, Gonze D, Barriol M, Dennis C, Goldbeter A, Dupont G, Chazaud C (2014) Gata6, Nanog and Erk signaling control cell fate in the inner cell mass through a tristable regulatory network. <i>Development</i> 141:3637-48 [<a href="Bessonnard2014.pdf">PDF</a>].
<li>Cang Z, Wang Y, Wang Q, Cho KWY, Holmes W, Nie Q (2021) A multiscale model via single-cell transcriptomics reveals robust patterning mechanisms during early mammalian embryo development. <i>PLoS Comput Biol</i> 17:e1008571 [<a href="https://pubmed.ncbi.nlm.nih.gov/33684098/">PubMed</a>].
<li>De Mot L, Gonze D, Bessonnard S, Chazaud C, Goldbeter A, Dupont G (2016) Cell fate specification based on tristability in the inner cell mass of mouse blastocysts. <i>Biophys J</i> 110:710-722 [<a href="DeMot2016.pdf">PDF</a>]. 
<li>Gardner TS, Cantor CR, Collins JJ (2000) Construction of a genetic toggle switch in Escherichia coli. <i>Nature</i>. 403:339-42 [<a href="Gardner2000.pdf">PDF</a>]. 
<li>Saiz N, Mora-Bitria L, Rahman S, George H, Herder JP, Garcia-Ojalvo J, Hadjantonakis AK (2020) Growth-factor-mediated coupling between lineage size and cell fate choice underlies robustness of mammalian development. <i>eLife</i> 9:e56079 [<a href="https://pubmed.ncbi.nlm.nih.gov/32720894/">PubMed</a>]. 
<li>Tosenberger A, Gonze D, Bessonnard S, Cohen-Tannoudji M, Chazaud C, Dupont G (2017) A multiscale model of early cell lineage specification including cell division. <i>NPJ Syst Biol Appl</i> 3:16 [<a href="Tosenberger2017.pdf">PDF</a>].
</ul>
</small>

<br><br><hr color="#0000DD" size=1>
<center><font size=-1>Didier Gonze - 8/11/2021 - School "<a href="https://project.inria.fr/compsysbio2021/">Computational Systems Biology</a>" - Aussois - November 2021 </font>
</td></tr></table>
</center>
<br>

</html>
