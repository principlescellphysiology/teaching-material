### Teaching materials "Systems biology"

* [Lecture slides "Introduction to systems biology"](SysBio_2020_Lecture_1_Introduction_to_systems_biology.pdf) (W. Liebermeister, CRI 2020) 
* [Lecture slides "High-throughput experiments - The omics revolution in Systems Biology"](SysBio_2022_Lecture_2_Jules_High-throughput-experiments.pdf) (M. Jules, CRI 2022) 
* [Lecture slides "From data to models in molecular biology (part 1)"](SysBio_2022_Lecture_5_Calzone_Martignetti_From_data_to_models_1.pdf) (L. Calzone and L. Martignetti, CRI 2022) 
* [Lecture slides "From data to models in molecular biology (part 2)"](SysBio_2022_Lecture_5_Calzone_Martignetti_From_data_to_models_2.pdf) (L. Calzone and L. Martignetti, CRI 2022) 
