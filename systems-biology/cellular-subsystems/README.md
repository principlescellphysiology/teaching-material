### Teaching materials "Models of cellular subsystems"

* [Course script on signaling pathways](Signaling_Pathways_WS2009.pdf) (HU Berlin Student course, 2009)
* [Course script on gene expression models](Gene_Expression_WS2004.pdf) (HU Berlin Student course, 2004)
* [Lecture script "Gene input functions"](./systems-biology/cellular-networks/Lecture_Modelling_of_cell_processes_2009/course_script/script_5_gene_input_functions.pdf) (W. Liebermeister, 2009) 
* [Course script on calcium dynamics](Calcium_Dynamics_WS2004.pdf) (HU Berlin Student course, 2004)
* [Course script on cell cycle models](Cell_Cycle_WS2009.pdf) (HU Berlin Student course, 2009)
* [Course script on circadian rhythms](Circadian_Rhythms_WS2007.pdf) (HU Berlin Student course, 2007)
* [Course script on neuronal dynamics](Neuronal_Dynamics_WS2007.pdf) (HU Berlin Student course, 2007)
* [Course script on DNA repair](DNA_Repair_SS2006.pdf) (HU Berlin Student course, 2006)
* [Course script on chemotaxis](Chemotaxis_WS2007.pdf) (HU Berlin Student course, 2007)
