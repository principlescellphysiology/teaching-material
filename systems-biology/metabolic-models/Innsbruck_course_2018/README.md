Blackboard teaching on "The protein cost of metabolic fluxes in bacteria"
-------------------------------------------------------------------------

* held by: W. Liebermeister
* held at: [Advanced lecture course on systems biology](https://sysbio-course.org/sysbio2018/info.php), Innsbruck 2018
* held in: Spring 2018
