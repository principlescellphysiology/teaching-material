Practical exercises: "Dynamic models integrating metabolism and gene expression"
-----------------------------------------------------------

* held by: H. de Jong
* held at: [CompSysBio - Advanced Lecture Course on Computational Systems Biology](https://project.inria.fr/compsysbio2021/), Aussois 2021
* held in: November 2021

------------------------------------------------------------
### Dynamic models integrating metabolism and gene expression

Hidde de Jong

INRIA Grenoble – Rhône-Alpes

We will step-by-step build quantitative ODE models of a metabolic network integrating regulation on both the metabolic and gene expression level, and investigate the effect of these layers of regulation on the networks dynamics. The course will be structured around the case of carbon catabolite repression in bacteria, using simple kinetic models. All simulations will be carried out by means of Matlab (no toolboxes needed). The hand-out and the models and simulation code are available from the instructor.

Kremling A, Geiselmann J, Ropers D, de Jong H (2015). Understanding carbon catabolite repression in Escherichia coli using quantitative models. Trends Microbiol., 23:99-109