# SysBio_SS2019

Exercises in the course of Systems Biology during Spring Semester 2019 at ETH

## Requirements
To run Python notebooks locally you need the following software

- Install [Anaconda](https://www.anaconda.com/distribution/) on your system
- Install [Git](https://git-scm.com/downloads)

## Setting up the environment
You need to install the required packages in Anaconda.
In an anaconda prompt type to following

    conda create --name sysbio

This will create a new evironment called sysbio, which you need to activate with

    activate sysbio

if this does not work try

    source activate sysbio

You then need to install some conda packages

    conda install numpy scipy matplotlib jupyter pip scikit-learn cython graphviz
    conda install -c alubbock pysb
    conda install -c anaconda pygraphviz

Also install some packages from pip

    pip install cobra escher

You can then change (in the conda prompt) to the directory you want to store
your files and pull down the repository with the command

    git clone https://gitlab.ethz.ch/darioce/sysbio_ss2019.git
    
Change to the directory and start the a jupyter notebook

    cd sysbio_ss2019
    jupyter notebook

You can now interactively code in your browser...

## Downloading new exercises

To obtain the latest version of all exercises, you can use the command

    git stash
    git pull
    git stash pop

To learn more about version control, have a look at the [documentation of Git](https://git-scm.com/docs/gittutorial)

## Online Notebooks

There are binders for each lession available under the following links:

0. [Python Tutorial](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.ethz.ch%2Fdarioce%2Fsysbio_ss2019.git/4c383c1e118678cf26ad8b6d9c0be7f41b15ad69?filepath=00_Tutorial)
1. [Python Exercise](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.ethz.ch%2Fdarioce%2Fsysbio_ss2019.git/4c383c1e118678cf26ad8b6d9c0be7f41b15ad69?filepath=01_Python_Exercises)
2. [Emergent Properties](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.ethz.ch%2Fdarioce%2Fsysbio_ss2019.git/4c383c1e118678cf26ad8b6d9c0be7f41b15ad69?filepath=02_Simulate_Petri_Dish)
3. [FBA small](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.ethz.ch%2Fdarioce%2Fsysbio_ss2019.git/fb8baba355c3f82631c9bc0fdf8b4f3f3a58e1fb?filepath=03_FBA_small)
4. [FBA E. coli](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.ethz.ch%2Fdarioce%2Fsysbio_ss2019.git/8bad4772a157e1f0e5cab8716bc140aa2ba29bda?filepath=04_FBA_E_coli)
5. [Enzyme Kinetics]
6. [Enzyme with Feedback]
7. [Model of glycolysis]
8. [Signaling]


**REMEBER!** CHANGES IN THE ONLINE BINDER WILL BE LOST
As soon as you close the browser window, all your changes will be gone.
Make sure you download the .ipynb file to your computer.
You can reupload your file next time you open the binder.