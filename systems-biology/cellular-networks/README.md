### Teaching materials "Cellular networks"

* [Course script on cellular networks](Cellular_Networks_WS2005.pdf) (HU Berlin Student course, 2005, 2005)
* [Lecture slides "Biochemical network models"](SysBio_2020_Lecture_2_Biochemical_network_models.pdf)  (W. Liebermeister, CRI 2020)
* [Blackboard session on qualitative dynamical modeling](logical-modeling) (D. Thieffry, blackboard session at CompSysBio2021 workshop) 
* [Lecture slides "Metabolic networks"](SysBio_2020_Lecture_3a_Metabolic_networks.pdf) (W. Liebermeister, CRI 2020)
* [Lecture script "Network motifs"](Lecture_Modelling_of_cell_processes_2009/course_script/script_4_network_motifs.pdf) (W. Liebermeister, 2009) 
* [Lecture slides "Network motifs"](SysBio_2020_Lecture_3b_Network_motifs.pdf) (W. Liebermeister, CRI 2020)
